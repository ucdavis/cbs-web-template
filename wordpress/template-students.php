<?php
/*
    CBS Site templates
    Copyright The Regents of the University of California, Davis 
    all rights reserved
    
    Designed and built by Information & Educational Technology
    University of California, Davis
    
    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to
    deal in the Software without restriction, including without limitation the
    rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
    sell copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
    IN THE SOFTWARE.
    
    Student page for special student information section.

    @package WordPress
    @subpackage CBS Web Template
    
    Template Name: Student Subsection

*/
enqueue('css/students.css');
function cbs_wordpress_student_head_background_url(){
global $post;
$url = wp_get_attachment_url(get_post_thumbnail_id($post->ID), 'full');
if($url){
?>
<style type = 'text/css'>
.page-lead {
    background-image: url('<?=$url;?>');
    background-size: 100%;
</style>
<?php
}}
add_action('wp_head', 'cbs_wordpress_student_head_background_url');
get_header(); ?>
<style>
</style>
<div class="page-lead">
    <div class="header-inner">
        <div class="page-info">
            <h2>Students</h2>
            <div class="page-info-contents">
            <dl class="dl-horizontal">
                <dt>visit:</dt>
                <dd>
                <div>
                    <span>202 Life Sciences Bldg<br>
                    Monday-Friday<br>
                    9am - 12pm<br>
                    1pm - 4pm
                    </span>
                </div>
                </dd>
                <dt>call:</dt>
                <dd>
                <div class="vcard">
                    <span class="tel"><span class="value">530-752-0410</span></span><br>
                </div>
                </dd>
                <dt>write:</dt>
                <dd><a href="mailto:cbs@ucdavis.edu">cbsundergrads@ucdavis.edu</a></dd>
            </dl>
            <a class="btn btn-styled btn-large btn-danger" href="#">Apply Online &raquo;</a>
            </div>
        </div>
    </div>
</div><!-- /.main header -->

<div id="homepage-sections" class="main content row">
    <section id="students" class="<?php cbs_wordpress_theme_columns();?>">
        <?php the_content();?>
    </section><!-- /.News -->
	<section id="sidebar-container" class="<?php cbs_wordpress_theme_columns();?>">
        <div id="cbs_wordpress_resources" class="widget sidebar">
            <h1 class="section-heading">Resources</h1>
            <div class="container-fluid">
            <div class="row-fluid">
            <div class="span6">
                <ul class="unstyled">
                    <li><a href="">Apply Online</a> </li>
                    <li><a href="">Admissions</a> </li>
                    <li><a href="">Catalog</a> </li>
                    <li><a href="">Schedules</a> </li>
                    <li><a href="">Registrar</a> </li>
                </ul>
            </div>
            <div class="span6">
                <ul class="unstyled">
                    <li><a href="">Financial aid</a> </li>
                    <li><a href="">Degree Navigator</a> </li>
                    <li><a href="">Assist</a> </li>
                    <li><a href="">Visitor Information</a> </li>
                    <li><a href="http://campusmap.ucdavis.edu/">Campus Map</a> </li>
                </ul>
            </div>
            </div>
        </div>
        <!-- News -->
        <div id="cbs_wordpress_student_news" class="widget sidebar">
            <h1 class="section-heading">Latest News</h1>
            <a class="rss-link" href="<?=$news_link . 'feed/';?>" alt="RSS feed">
            <img src="<?php _i();?>/img/rss.png" alt="Link to news RSS feed"></a>
            <a href="<?=$news_link;?>" class="more-link">More News &rarr;</a>  
            <!--News loop-->
            <?php
                $news_cat = get_category_by_slug('news');
                $news_cat_id = $news_cat ? $news_cat->cat_ID : '';
                $news_link = get_category_link($news_cat_id);
                $query = new WP_Query(array('cat' => $news_cat_id, 'posts_per_page' => 2));
                if($query->have_posts()):
                    while($query->have_posts()):
                        $query->the_post();
            ?>
                <article id="news-<?php the_id(); ?>" class="news-summary">
                    <header>
                        <a href="<?php the_permalink(); ?>">
                            <?php if(has_post_thumbnail()):?>
                            <?php the_post_thumbnail();?>
                            <?php endif;?>
                            <h2><?=get_the_title();?></h2>
                        </a>
                    </header>
                    <p class="byline"><?php cbs_wordpress_entry_meta();?></p>
                    <?php the_excerpt();?>
                    <p><a href="<?php the_permalink();?>">Read More &rarr;</a></p>
                </article>
            <?php      
                    
                    endwhile;   // end loop       ?>
                <!-- /.News loop -->
                <a class="more-link" href="<?=$news_link;?>">More news &rarr;</a>
            <?php  else:   ?>
                <p>No news at this time.</p>
            <?php  endif;  // end if have_posts      ?>
        </div>
	</section>
    </div>
<?php get_footer();
