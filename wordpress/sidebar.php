<?php
/**
 * The sidebar containing the main widget area.
 *
 * If no active widgets in sidebar, let's hide it completely.
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */
if (!is_active_sidebar('sidebar-home') && !is_active_sidebar('sidebar-article'))
	return;
?>
	<section id="sidebar-container" class="<?php cbs_wordpress_theme_columns();?>">
		<?php 
		if(is_front_page() && is_active_sidebar('sidebar-home')):
    		dynamic_sidebar('sidebar-home');
		else:
            if(is_single() && is_active_sidebar('sidebar-article'))
		        dynamic_sidebar('sidebar-article');
            if(is_page() && is_active_sidebar('sidebar-default'))
                dynamic_sidebar('sidebar-default');
		endif;
		?>
	</section>
