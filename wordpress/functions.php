<?php
/*
    CBS Site templates
    Copyright The Regents of the University of California, Davis 
    all rights reserved
    
    Designed and built by Information & Educational Technology
    University of California, Davis
    
    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to
    deal in the Software without restriction, including without limitation the
    rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
    sell copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
    IN THE SOFTWARE.

    Functions and definitions

    Sets up the theme and provides helper functions, which are used
    in the theme as custom template tags. Others are attached to action and
    filter hooks in WordPress to change core functionality. Based in large part
    off of the twentytwelve theme which comes packaged with WordPress core.

    When using a child theme (see http://codex.wordpress.org/Theme_Development 
    and http://codex.wordpress.org/Child_Themes), you can override certain 
    functions (those wrapped in a function_exists() call) by defining them first 
    in your child theme's functions.php file. The child theme's functions.php 
    file is included before the parent theme's file, so the child theme 
    functions would be used.

    Functions that are not pluggable (not wrapped in function_exists()) are 
    instead attached to a filter or action hook.

    For more information on hooks, actions, and filters, 
    see http://codex.wordpress.org/Plugin_API.

    @package WordPress
    @subpackage CBS Web Template

*/

define('TEMPLATE', get_template_directory_uri());
define('DIR', get_template_directory());
define('DOMAIN', 'cbs_wordpress');
define('ERR', DOMAIN . '_admin_errors');

#    Change this setting to branches in the bitbucket repository of the theme
#    For example, to pull from the dev branch, put in 'dev'
if(!defined('SOURCE_BRANCH'))
    define('SOURCE_BRANCH', 'master');


// Includes
include(DIR . '/inc/bootstrap_walker.php');
include(DIR . '/inc/class_registry.php');
include(DIR . '/inc/theme_settings.php');
include(DIR . '/inc/theme_updater.php');
include(DIR . '/inc/widgets.php');
include(DIR . '/inc/acf_fields.php');

// library which lets us require plugins
require_once(DIR . '/inc/plugin_installer/tgm-plugin-activation/class-tgm-plugin-activation.php');

// instantiate the registry
$reg = reg();

// Social media settings
$reg->set('social_media', array(
    'facebook'  => 'facebook.png',
    'google+'   => 'gplus2.png',
    'twitter'   => 'twitter.png',
    'linkedin'  => 'linkedin.png',
));

/*******************************************************************************
    Register sidebars
*******************************************************************************/
function cbs_wordpress_widgets_init() {
    register_sidebar(array(
        'name' => __('Pages Sidebar', DOMAIN),
        'id' => 'sidebar-default',
        'description' => __('Default sidebar for pages', 
        DOMAIN),
        'before_widget' => '<div id="%1$s" class="widget sidebar %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h1 class="section-heading">',
        'after_title' => '</h1>',
    ));
    register_sidebar(array(
        'name' => __('Home Sidebar', DOMAIN),
        'id' => 'sidebar-home',
        'description' => __('Sidebar appearing on the homepage only', 
        DOMAIN),
        'before_widget' => '<div id="%1$s" class="widget sidebar %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h1 class="section-heading">',
        'after_title' => '</h1>',
    ));
    register_sidebar(array(
        'name' => __('Posts Sidebar', DOMAIN),
        'id' => 'sidebar-article',
        'description' => __('Appears in articles. Houses article-specific widgets', 
        DOMAIN),
        'before_widget' => '<div id="%1$s" class="widget sidebar %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h1 class="section-heading">',
        'after_title' => '</h1>',
    ));
}
add_action( 'widgets_init', 'cbs_wordpress_widgets_init' );

/*******************************************************************************
    Setup theme defaults
*******************************************************************************/
function cbs_wordpress_setup() {

    // Adds RSS feed links to <head> for posts and comments.
    add_theme_support('automatic-feed-links');

    // To support a variety of formats, add them here.
#    add_theme_support('post-formats', array('gallery'));

    // This theme uses wp_nav_menu() in one location.
#    register_nav_menu( 'primary', __( 'Primary Menu', DOMAIN ) );

    // This theme uses a custom image size for featured images, displayed on "standard" posts.
    add_theme_support('post-thumbnails');

    // This theme uses wp_nav_menu(). Add any additional menus here
    register_nav_menu( 'primary', __( 'Main Navigation Menu', DOMAIN ) );
    register_nav_menu( 'students', __( 'Students subsection menu', DOMAIN ) );
    
    
    // Create categories used to operate News, Events, and Highlights
    $default_categories = array(
        'news'          =>  array(
                        'cat_name' => __('News', DOMAIN),
                        'category_description' => __(
                        'News stories to appear on the front page of the site', DOMAIN),
                        'taxonomy' => 'category',
                        ),
        'events'        =>  array(
                        'cat_name' => __('Events', DOMAIN),
                        'category_description' => __(
                        'Events with a start date, an optional end date, and a location', DOMAIN),
                        'taxonomy' => 'category',
                        ),
        'highlights'    =>  array(
                        'cat_name' => __('Highlights', DOMAIN),
                        'category_description' => __(
                        'Add this category to a post to enable its use in the content rotator', DOMAIN),
                        'taxonomy' => 'category',
                        ),
    );
    foreach($default_categories as $key => $catarr):
        $catarr['category_nicename'] = $key;
        if(!get_category_by_slug($key)):
            if (file_exists (ABSPATH.'/wp-admin/includes/taxonomy.php')) {
                require_once (ABSPATH.'/wp-admin/includes/taxonomy.php');
                wp_insert_category($catarr);
            }
        endif;
    endforeach;
}
add_action( 'after_setup_theme', 'cbs_wordpress_setup' );

/*******************************************************************************
    Enqueue scripts and styles
*******************************************************************************/
function cbs_wordpress_scripts_styles() {
    $protocol = is_ssl() ? 'https' : 'http';

    //  Add jquery
    wp_enqueue_script('jquery');

    //  Adds JavaScript for handling the navigation menu hide-and-show behavior.
    wp_enqueue_script('cbs_wordpress-bootstrap', TEMPLATE . '/js/bootstrap.min.js', array(), '', true );

    //  Adds JavaScript for handling the navigation menu hide-and-show behavior.
    wp_enqueue_script('cbs_wordpress-modernizr', TEMPLATE . '/js/modernizr-2.6.2.min.js', array(), '', true );
    
    /*
        Loads our special font CSS file.
        Sites need to be activated by the administrator of the fontdeck.com
        account. To request enabling site fonts, please contact CBS directly.
    */
    $siteurl = parse_url(get_option('siteurl'), PHP_URL_HOST);
    $fonts_css = "$protocol://f.fontdeck.com/s/css/l4MsFun30OJhB9DTDGcb8JQSAOQ/$siteurl/31819.css";
    wp_enqueue_style('cbs_wordpress-fonts', $fonts_css, array(), null);

    //  Loads our main stylesheet.
    wp_enqueue_style('cbs_wordpress-style', get_stylesheet_uri());

    //  Loads the responsive stylesheet.
    $responsive_css = TEMPLATE . '/css/responsive.css';
    wp_enqueue_style('cbs_wordpress-responsive', $responsive_css, array('cbs_wordpress-style'), null);

}
add_action('wp_enqueue_scripts', 'cbs_wordpress_scripts_styles');

/*******************************************************************************
    Enqueue supplementary styles
*******************************************************************************/
function enqueue($file, $type = false, $handle = false){
    $file = strpos($file, '/') === 0 ? $file : get_stylesheet_directory() . '/' . $file;
    $path = pathinfo(realpath($file));
    if(!file_exists($file))
        return false;
    $type = $type ? $type : $path['extension'];
    $handle = $handle ? $handle : sprintf("cbs_wordpress-%s-%s", $type, $path['filename']);
    $url = array(TEMPLATE, $type, $path['basename']);
    $url = join($url, "/");
    if($type == 'js')
        wp_enqueue_script($handle, $url, array(), '', true);
    else
        wp_enqueue_style($handle, $url, array(), null);
}
function cbs_wordpress_enqueue_post_resources(){
/*  
    We have different types of resources we want to enqueue
    *   front pages should have a home.css
    *   articles should have an article.css
    *   post_formats probably have their own $post_format files (css & js)
*/
    global $post;
    if(is_front_page()){
        enqueue('css/home.css');
        if(cbs_wordpress_has_carousel())
            enqueue('js/carousel_init.js');
    }
    if(is_single()){
        enqueue('css/article.css');
    }
    if(get_post_format()){
        $format = get_post_format();
        enqueue("css/$format.css");
        enqueue("js/$format.js");
    }
}
add_action('wp_enqueue_scripts', 'cbs_wordpress_enqueue_post_resources');

/*******************************************************************************
    Updater service for theme
*******************************************************************************/
add_action('after_setup_theme', 'cbs_wordpress_updater_service');
function cbs_wordpress_updater_service(){
    new WPBitbucketThemeUpdate(SOURCE_BRANCH);
}

/*******************************************************************************
    Theme setting register
*******************************************************************************/
add_action('customize_register', 'cbs_wordpress_customize_register');

/*******************************************************************************
    Output pretty title, based on location
*******************************************************************************/
function cbs_wordpress_filter_title( $title, $sep ) {
    global $paged, $page;

    if ( is_feed() ) {
        return $title;
    }

    // Add the site name.
    $title .= get_bloginfo( 'name' );

    // Add the site description for the home/front page.
    $site_description = get_bloginfo( 'description', 'display' );
    if ( $site_description && ( is_home() || is_front_page() ) ) {
        $title = "$title $sep $site_description";
    }

    // Add a page number if necessary.
    if ( $paged >= 2 || $page >= 2 ) {
        $title = "$title $sep " . sprintf( __( 'Page %s', DOMAIN ), max( $paged, $page ) );
    }

    return $title;
}
add_filter('wp_title', 'cbs_wordpress_filter_title', 10, 2);

//  Function to get page slug
//  courtesy: http://www.tcbarrett.com/2011/09/wordpress-the_slug-get-post-slug-function/
function the_slug($echo=true){
    $slug = basename(get_permalink());
    do_action('before_slug', $slug);
    $slug = apply_filters('slug_filter', $slug);
    if( $echo ) echo $slug;
    do_action('after_slug', $slug);
    return $slug;
}
function get_the_slug(){
    return the_slug(false);
}
/*******************************************************************************
    Setup navigation
*******************************************************************************/
if (!function_exists('cbs_wordpress_main_nav')):
function cbs_wordpress_main_nav() {
    $slug = get_the_slug();
    $page_nav = has_nav_menu($slug) ? $slug : 'primary';
    if(has_nav_menu($page_nav)):
        wp_nav_menu(array(
            'theme_location' => $page_nav,
            'container' => false,
            'menu_class' => 'nav nav-menu',
            'walker' => new Bootstrap_Walker_Nav_Menu()
        ));
    endif;
}
endif;

/*******************************************************************************
    Makes our wp_nav_menu() fallback -- wp_page_menu() -- show a home link.
*******************************************************************************/
function cbs_wordpress_page_menu_args( $args ) {
    if ( ! isset( $args['show_home'] ) )
        $args['show_home'] = true;
    return $args;
}
add_filter( 'wp_page_menu_args', 'cbs_wordpress_page_menu_args' );

/*******************************************************************************
    Format article ids
*******************************************************************************/
function cbs_wordpress_article_id($echo = true) {
    global $paged, $page, $post;
    $output_array[] = is_page($post) ? "page" : "post";
    $output_array[] = $post->ID;
    if($paged)
        $output_array[] = $page;
    $output = join('-', $output_array);
    if($echo)
        echo $output;
    else
        return $output;
}

/*******************************************************************************
    Check if page has a carousel
*******************************************************************************/
function cbs_wordpress_carousel_options(){
    $options = get_option("cbs_wordpress_content_rotator");
    return $options['rotator_type'] == 'none' ? false : $options;
}

function cbs_wordpress_get_carousel_css(){
    if(cbs_wordpress_has_carousel() && cbs_wordpress_carousel_nav() != false):
        $out = array();
        $carousel = cbs_wordpress_get_carousel();
        foreach($carousel as $key => $c):
            if(!empty($c['css'])):
                $out[$key] = sprintf("/* CSS for %s */\n", get_permalink($key)) . $c['css'];
            endif;
        endforeach;
        return $out;
    endif;
    return array();
}

function cbs_wordpress_get_carousel(){
    $carousel = wp_cache_get('carousel') ? wp_cache_get('carousel') : array();
    if(empty($carousel)):
        $carousel_query = new WP_Query(array(
            'posts_per_page'    => 5,
            'category_name'     => 'highlights',
            'has_thumbnail'     => true,
        ));
        while($carousel_query->have_posts()):
            $carousel_query->the_post();
            $id = get_the_ID();
            
            $title = get_field('title') ? get_field('title') : get_the_title();
            $description = get_field('description') ? get_field('description') : get_the_excerpt();
            $description = html_entity_decode($description);
            $link =  "<a class='btn btn-primary' href='" . get_permalink($key) . "'>Learn more</a>";
            $description = str_replace('[…]', '…<br/>' . $link, $description);
            $css = get_field('custom_css');
            $compiled_css = "";
            if($css){
                require_once(DIR . "/inc/lessphp/lessc.inc.php");
                $cssstr = "#carousel-$id {\n%s\n}\n";
                $less = new lessc;
                try {
                    $compiled_css = $less->compile(sprintf($cssstr, $css));
                } catch (Exception $e) {
                    $error = $e->getMessage();
                }
            }
            $img = has_post_thumbnail() ? get_the_post_thumbnail() : false;

            $carousel[$id]['post'] = $carousel_item;
            $carousel[$id]['fields'] = $fields;
            $carousel[$id]['title'] = $title;
            $carousel[$id]['description'] = $description;
            $carousel[$id]['img'] = $img;
            $carousel[$id]['css'] = $compiled_css;
        endwhile;
        wp_reset_postdata();
    endif;
    wp_cache_set('carousel', $carousel, false, 3600);
    return empty($carousel) ? false : $carousel;
}

function cbs_wordpress_has_carousel(){
    return cbs_wordpress_carousel_options() != false && cbs_wordpress_get_carousel();
}

function cbs_wordpress_carousel_nav_class(){
    global $has_carousel;
    $has_carousel = isset($has_carousel) && !empty($has_carousel) ? true : false;
    if(cbs_wordpress_carousel_nav() || $has_carousel)
        return " with-carousel";
    return false;
}

function cbs_wordpress_carousel_logo_class(){
    if(cbs_wordpress_carousel_nav_class()) return " with-overlay";
    return false;
}

function cbs_wordpress_carousel_nav(){
    return cbs_wordpress_has_carousel() && (is_front_page() || is_home());
}

function cbs_wordpress_show_carousel(){
    return cbs_wordpress_carousel_nav() != false;
}

function cbs_wordpress_output_carousel_css(){
    $carousel_css = cbs_wordpress_get_carousel_css();
    if(!empty($carousel_css)):
        echo "<style>\n@media (min-width: 768px) {\n";
        foreach(cbs_wordpress_get_carousel_css() as $key => $css){
            echo $css . "\n";
        }
        echo "}</style>\n";
    endif;
}
add_action('wp_head', 'cbs_wordpress_output_carousel_css');

/*******************************************************************************
    Byline
*******************************************************************************/
if (!function_exists('cbs_wordpress_entry_meta')):
function cbs_wordpress_entry_meta() {
    // Translators: used between list items, there is a space after the comma.
    $categories_list = get_the_category_list(__(', ', DOMAIN));

    // Translators: used between list items, there is a space after the comma.
    $tag_list = get_the_tag_list('', __(', ', DOMAIN));

    $list = $tag_list ? $tag_list : $categories_list;
    $list = $list ? $list : false;

    $date = sprintf('<a href="%1$s" title="%2$s" rel="bookmark"><time class="entry-date" datetime="%3$s">%4$s</time></a>',
        esc_url(get_permalink()),
        esc_attr(get_the_time()),
        esc_attr(get_the_date('c')),
        esc_html(get_the_date())
    );

    $author = sprintf('<span class="author vcard"><a class="url fn n" href="%1$s" title="%2$s" rel="author">%3$s</a></span>',
        esc_url(get_author_posts_url(get_the_author_meta('ID'))),
        esc_attr(sprintf(__('View all posts by %s', DOMAIN), get_the_author())),
        get_the_author()
    );

    $utility_text = 'by %1$s published on %2$s. ';
    if($list)
        $utility_text .= sprintf('Posted in %1$s. ', $list);

    //  Request came in to change this to just the publication date
    //  The functionality of this method is solid, so leaving the code here.
    //  To re-enable author name and cat/tag list, comment out the following line
    $utility_text = 'Published on %2$s.';

    if(!is_page()){
        printf(
            $utility_text,
            $author,
            $date
        );
    }
}
endif;
/*******************************************************************************
    Contact info
*******************************************************************************/
if (!function_exists('cbs_wordpress_contact_page_link')):
function cbs_wordpress_contact_page_link(){
    $contact = get_pages(array('name' => 'contact', 'post_type' => 'page')) or false;
    return $contact ? get_page_link($contact[0]->ID) : '';
}
endif;
if (!function_exists('cbs_wordpress_contactinfo_display')):
function cbs_wordpress_contactinfo_display() {
    $contact = new CBS_Contactinfo();
    $contact->render();
}
endif;

/*******************************************************************************
    Utility functions
*******************************************************************************/
//  Shortcut to output directory for template use
function _i(){
    echo get_template_directory_uri();
}
function show_admin_message($message, $type = 'default', $page = false){
    global $pagenow;
    $types = array(
    'default' => 'strong',
    'warning' => 'updated',
    'error' => 'error'
    );
    $class = array_key_exists($type, $types) ? $types[$type] : $types['default'];
    $message = esc_html($message);
    if($page && $page != $pagenow)
        return false;
    if($class == 'default')
        echo "<p><strong>$message</strong></p></div>";
    else
        echo '<div id="message" class="' . $class . '">' . $message . '</div>';
}
/*
    go through an array of arrays and look for $key, pull out that value and 
    add it to a 2d array. Optionally using $new_key_value value as the new key
    for the array
*/
if(!function_exists('array_pull')):
function array_pull($array, $key, $new_key_value = false){
    $out = array();
    foreach($array as $k => $a){
        if(!is_array($a)) continue;
        if($new_key_value != false && !isset($a[$new_key_value])) continue;
        if(!isset($a[$key])) continue;
        $new_key = $new_key_value ? $a[$new_key_value] : $k;
        $out[$new_key] = $a[$key];
    }
    return $out;
}
endif;
/*
    Get category link by slug backed by cache (since data changes rarely)
*/
function get_category_link_by_slug($slug){
    $cache_name = __FUNCTION__ . "_" . $slug;
    $link = wp_cache_get($cache_name);
    if(!$link){
        $cat = get_category_by_slug($slug);
        $cat_id = $cat ? $cat->cat_ID : '';
        $link = get_category_link($cat_id);
        wp_cache_set($cache_name, $link);
    }
    return $link;
}
function cbs_wordpress_admin_errors(){
    settings_errors(ERR, true, false);
}
add_action('admin_notices', 'cbs_wordpress_admin_errors');
/*******************************************************************************
    Require use of ACF plugin
*******************************************************************************/
function cbs_wordpress_setup_plugins(){
    $plugins = array(
#        We require the following plugins to operate the features of the theme
        array(
            'name'         => 'Advanced Custom Fields',
            'slug'         => 'advanced-custom-fields',
            'required'     => true,
        ),
        array(
            'name'         => 'Advanced Custom Fields: Date and Time Picker Field',
            'slug'         => 'acf-field-date-time-picker',
            'required'     => true,
        ),
        array(
            'name'         => 'Page Links To',
            'slug'         => 'page-links-to',
            'required'     => false,
        ),
        array(
            'name'          => 'Unattach',
            'slug'          => 'unattach',
            'required'      => true,
        ),
#        We also recommend the following plugins
#        *   Some sort of caching, such as WP Super cache
        array(
            'name'         => 'WP Super Cache',
            'slug'         => 'wp-super-cache',
            'required'     => false,
        ),
    );

    $config = array(
        // Text domain - likely want to be the same as your theme.
        'domain' => DOMAIN, 
        // Default absolute path to pre-packaged plugins
        'default_path' => '',
        // Default parent menu slug
        'parent_menu_slug' => 'themes.php',
        // Default parent URL slug
        'parent_url_slug' => 'themes.php',
        // Menu slug
        'menu' => 'install-required-plugins',
        // Show admin notices or not
        'has_notices' => true,
        // Automatically activate plugins after installation or not
        'is_automatic' => true,
        // Message to output right before the plugins table
        'message' => '',
        'strings' => array(
            'page_title' => __( 'Install Required & Recommended Plugins', DOMAIN ),
            'menu_title' => __( 'Install Plugins', DOMAIN ),
            'installing' => __( 'Installing Plugin: %s', DOMAIN ), // %1$s = plugin name
            'oops' => __( 'Something went wrong with the plugin API.', DOMAIN ),
            'notice_can_install_required' => _n_noop( 'The following plugin is required to operate the CBS Wordpress theme: %1$s.', 'The following plugins are required to operate the CBS Wordpress theme: %1$s.' ), // %1$s = plugin name(s)
            'notice_can_install_recommended' => _n_noop( 'We recommend the following plugin: %1$s.', 'We recommends the following plugins: %1$s.' ), // %1$s = plugin name(s)
            'notice_cannot_install' => _n_noop( 'Sorry, but you do not have the correct permissions to install the %s plugin. Contact the administrator of this site for help on getting the plugin installed.', 'Sorry, but you do not have the correct permissions to install the %s plugins. Contact the administrator of this site for help on getting the plugins installed.' ), // %1$s = plugin name(s)
            'notice_can_activate_required' => _n_noop( 'The following required plugin is currently inactive: %1$s.', 'The following required plugins are currently inactive: %1$s.' ), // %1$s = plugin name(s)
            'notice_can_activate_recommended' => _n_noop( 'The following recommended plugin is currently inactive: %1$s.', 'The following recommended plugins are currently inactive: %1$s.' ), // %1$s = plugin name(s)
            'notice_cannot_activate' => _n_noop( 'Sorry, but you do not have the correct permissions to activate the %s plugin. Contact the administrator of this site for help on getting the plugin activated.', 'Sorry, but you do not have the correct permissions to activate the %s plugins. Contact the administrator of this site for help on getting the plugins activated.' ), // %1$s = plugin name(s)
            'notice_ask_to_update' => _n_noop( 'The following plugin needs to be updated to its latest version to ensure maximum compatibility with this theme: %1$s.', 'The following plugins need to be updated to their latest version to ensure maximum compatibility with this theme: %1$s.' ), // %1$s = plugin name(s)
            'notice_cannot_update' => _n_noop( 'Sorry, but you do not have the correct permissions to update the %s plugin. Contact the administrator of this site for help on getting the plugin updated.', 'Sorry, but you do not have the correct permissions to update the %s plugins. Contact the administrator of this site for help on getting the plugins updated.' ), // %1$s = plugin name(s)
            'install_link'  => _n_noop( 'Begin installing plugin', 'Begin installing plugins' ),
            'activate_link' => _n_noop( 'Activate installed plugin', 'Activate installed plugins' ),
            'return' => __( 'Return to Required Plugins Installer', DOMAIN ),
            'plugin_activated' => __( 'Plugin activated successfully.', DOMAIN ),
            'complete' => __( 'All plugins installed and activated successfully. %s', DOMAIN ), // %1$s = dashboard link
            'nag_type' => 'updated' // Determines admin notice type - can only be 'updated' or 'error'
        )
    );

    tgmpa($plugins, $config);
}
add_action('tgmpa_register', 'cbs_wordpress_setup_plugins');
