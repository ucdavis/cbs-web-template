<?php
/*******************************************************************************
    Contact info class
    We want to default to the main CBS information if they don't put anything
    here. Otherwise, we pull up whatever they want to show.
*******************************************************************************/

if(!defined('CBS_CI_SETTING_PREFIX'))
    define(CBS_CI_SETTING_PREFIX, 'cbs_wordpress_contactinfo');
require_once("simple-settings/wordpress-simple-settings.php");

class CBS_Contactinfo extends WordPress_SimpleSettings {
    public $prefix = CBS_CI_SETTING_PREFIX;
    public $settings_page = 'Contact Info';
    public $permissions = 'manage_options';
    public $defaults = array(
        'address' => array(
            'name'             => "College of Biological Sciences (CBS) Deans' Office",
            'street-address'    => 'One Shields Avenue',
            'locality'          => 'Davis',
            'region'            => 'California',
            'postal-code'       => '95616',
        ),
        'email' => array(
            'cbs@ucdavis.edu' => 'cbs@ucdavis.edu',
        ),
    );
    public $address = array(
        'name' => "Department name",
        'street-address' => 'Street address',
        'locality' => 'City',
        'region' => 'State',
        'postal-code' => 'Zip code'
    );
    public $phones_allowed = 3;
    function __construct(){
        parent::__construct();
        add_action('admin_menu', array($this, 'admin_menu'));
        register_activation_hook(__FILE__, array($this, 'activate') );
    }
    function set($sub, $setting){
        return join('_', array($this->prefix, $sub, $setting));
    }
    function admin_menu(){
        $this->page = add_options_page(
            $this->settings_page,
            $this->settings_page,
            $this->permissions,
            $this->prefix,
            array($this, 'admin_page')
        );
    }
    function activate(){
        foreach($this->address as $key => $val)
            $this->add_setting("address_$key", $this->defaults['address'][$key]);
        for($i=1; $i<=$this->phones_allowed; $i++)
            $this->add_setting("phone_$i", false);
        $this->add_setting("email_address", key($this->defaults['email']));
        $this->add_setting("email_label", $this->defaults['email']);
    }
    
    function admin_page(){?>
    <div class="wrap">
        <h2><?=$this->settings_page;?></h2>
        <p>Enter your group's contact information. This data will appear at the bottom of your site.</p>
        <form method="post" action="<?=$_SERVER['REQUEST_URI'];?>">
        	<?php $this->the_nonce(); ?>
        	<table class="form-table">
			    <tbody>
			        <tr>
			            <th scope="row" valign="top">Address</th>
			            <td>
			                <?php foreach($this->defaults['address'] as $key => $val):?>
						    <label>
							    <input type="text" 
							        name="<?=$this->get_field_name('address_' . $key); ?>" 
							        value="<?=$this->get_setting('address_' . $key); ?>" 
							     />&nbsp;<?=$this->address[$key];?>
						    </label><br/>
						    <?php endforeach;?>
					    </td>
				    </tr>
				    <tr>
					    <th scope="row" valign="top">Phone numbers</th>
					    <td>
						    Enter up to <?=$this->phones_allowed;?> phone numbers.<br/>
						    Feel free to label them (e.g. "Main line: XXX-XXX-XXXX, Fax: XXX-XXX-XXXX)<br/>
					        <?php for($i = 1; $i <= $this->phones_allowed; $i++):?>
						    <label>
							    <input type="text" 
							        name="<?=$this->get_field_name('phone_' . $i); ?>" 
							        value="<?=$this->get_setting('phone_' . $i); ?>" 
							     />&nbsp;Phone #<?=$i;?><br />
						    </label>
						    <?php endfor;?>
					    </td>
				    </tr>
				    <tr>
					    <th scope="row" valign="top">Email address</th>
					    <td>
						    Address:&nbsp;<input type="text" 
						        name="<?=$this->get_field_name('email_address'); ?>" 
						        value="<?=$this->get_setting('email_address'); ?>" 
						        placeholder="<?=key($this->defaults['email']);?>"
						     />&nbsp;
						    Label (optional):&nbsp;<input type="text" 
						        name="<?=$this->get_field_name('email_label'); ?>" 
						        value="<?=$this->get_setting('email_label'); ?>" 
						        placeholder="Admissions"
						     />&nbsp;
					    </td>
				    </tr>	
			    </tbody>
        	</table>
        	<?=submit_button();?>
        </form>
    </div>
    <?php
    }
    function render(){?>
    <dl class="contact-info dl-horizontal">
    <?php    
        $address = array();
        foreach($this->address as $key => $val)
            $address[$key] = $this->get_setting("address_" . $key, false);
        $phones = array();
        for($i = 0; $i < $this->phones_allowed; $i++)
            $phones[] = $this->get_setting("phone_" . $i, false);
        $phones = array_filter($phones);
        $email_address = $this->get_setting("email_address", 0);
        $email_label = $this->get_setting("email_label", false);
        $email[$email_address] = $email_label ? $email_label : $email_address;
        
        $has_email = array_filter($email);
        $has_phones = array_filter($phones);
        $has_address = array_filter($address);
        
        if(!empty($has_address)):
        ?>
        <dt>visit:</dt>
        <dd>
        <div class="adr">
            <?php foreach($address as $key => $val):
            
            ?>
            <span class="p-<?=$key;?> <?=$key;?>"><?=$val;?></span>
            <?php if(in_array($key, array('name', 'street-address'))){ echo "<br/>"; }
            endforeach;?>
        </div>
        </dd>
        <?php 
        endif;
        if(!empty($has_phones)):?>
        <dt>call:</dt>
        <dd>
        <div class="vcard">
        <?php
        foreach($phones as $key => $val):
        ?>
            <span class="tel"><?=$val;?></span><br>
        <?php endforeach;?>
            <a href="<?=cbs_wordpress_contact_page_link();?>">Other numbers</a>
        </div>
        </dd>
        <?php
        endif;
        if(!empty($has_email)):?>
        <dt>email:</dt>
        <dd><a href="<?=$email_address;?>"><?=$email_label;?></a></dd>
        <?php endif;?>
    </dl>
        <?php
    }
}
$CBS_Contactinfo = new CBS_Contactinfo();
