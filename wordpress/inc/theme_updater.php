<?php
#    Bitbucket based auto update class
class WPBitbucketThemeUpdate {

    protected $check_commit;
    protected $check_remote_version;
    
    protected $_o = DOMAIN;
    private $_e = 'theme_updater_error';
    protected $name = TEMPLATE_NAME;
    public $bitbucket_project = 'cbs-web-template';
    public $api = '2.0';

    public $branch = 'master';
    public $project = DOMAIN;
    public $owner = 'ucdavis';
    
    public $url_themedata   = false;
    public $url_download    = false;
    private $url_commits    = "https://bitbucket.org/api/%s/repositories/%s/%s/commits/%s";
                            //  api, owner, bitbucket_project, branch
    private $url_raw        = "https://bitbucket.org/%s/%s/raw/%s/%s";
                            //  owner, bitbucket_project, branch, file/directory
    
#    Cribbed from WP_Theme class. Why this is private, I'll never know
    public static $file_headers = array(
        'Name'        => 'Theme Name',   
        'ThemeURI'    => 'Theme URI',
        'Description' => 'Description',
        'Author'      => 'Author',
        'AuthorURI'   => 'Author URI',
        'Version'     => 'Version',
        'Template'    => 'Template',
        'Status'      => 'Status',
        'Tags'        => 'Tags',
        'TextDomain'  => 'Text Domain',
        'DomainPath'  => 'Domain Path',
    );
    
    function __construct($branch = false, $project = false, $owner = false, $check_commit = false){
        //  required by the http/https sanity check for loading remote files
        //  since this is loaded before error settings, we're making them available here
        require_once(ABSPATH . 'wp-admin/includes/template.php');

        $this->branch   = $branch ? $branch : get_option($this->_o . '_branch', $this->branch);
        $this->project  = $project ? $project : get_option($this->_o . '_project', $this->project);
        $this->owner    = $owner ? $owner : get_option($this->_o . '_owner', $this->owner);

        $this->hash     = get_option($this->_o . '_hash');
        $this->date     = get_option($this->_o . '_date');

        $this->check_commit = $check_commit;
        $this->check_remote_version = $this->check_commit === false;
        
        $theme = wp_get_theme($this->project);

        $this->local = array(
            'owner'     => $this->owner, 
            'project'   => $this->project, 
            'branch'    => $this->branch, 
            'hash'      => $this->hash, 
            'date'      => $this->date,
            'Version'	=> $theme->get('Version'),
            'ThemeURI'	=> $theme->get('ThemeURI')
        );

        $this->remote = array();
        $this->remote_fetched = false;

        $this->url_commits = sprintf(
            $this->url_commits, 
            $this->api, 
            $this->owner, 
            $this->bitbucket_project, 
            $this->branch
        );
        $this->url_download = sprintf(
            $this->url_raw,
            $this->owner,
            $this->bitbucket_project,
            $this->branch,
            'wordpress.zip'
        );
        $this->url_themedata = sprintf(
            $this->url_raw,
            $this->owner,
            $this->bitbucket_project,
            $this->branch,
            'wordpress.txt'
        );
        
        // sanity check to make sure the PHP install can fetch http files, 
        // since Wordpress itself doesn't do this
        $wrappers = stream_get_wrappers();
        if(!in_array('http', $wrappers) || !in_array('https', $wrappers))
            $this->error('stream_wrapper_error', 
            new WP_Error($this->_e, 'Your PHP version does not support file fetching via http/https. Check your openssl extension.'));

        // define the alternative API for updating checking
        add_filter('pre_set_site_transient_update_themes', array(&$this, 'check_update'));
    }
    
    function check_update($transient){
        if(!$transient->checked)
            return $transient;

        $checks = array();
        if($this->check_commit){$checks['commit'] = $this->validate_commit();}
        if($this->check_remote_version){$checks['version'] = $this->validate_remote_version();}

        $new_version = in_array(true, $checks);

        if($new_version){
            if(!$this->remote_fetched)
                $this->get_remote_themedata();
            $version = $this->local['Version'] == $this->remote['Version'] ? 
                $this->increment_version_number($this->local['Version']) : $this->remote['Version'];
                
#            Write necessary entry for the transient
            $obj['new_version'] = $version;
            $obj['url'] = $this->remote['ThemeURI'];
            $obj['package'] = $this->url_download;
            $transient->response[$this->project] = $obj;
        }
        return $transient;
    }
    
    function validate_remote_version(){
        $this->remote = array_merge($this->remote, $this->get_remote_themedata());
        return version_compare($this->local['Version'], $this->remote['Version'], '<');
    }

    function get_remote_themedata(){
        if($this->remote_fetched == true)
            return $this->remote;
        $theme = get_file_data($this->url_themedata, self::$file_headers);
        $theme_empty = array_filter($theme);
        if(empty($theme_empty)){
            return $this->error('fail_to_fetch', 
            new WP_Error($this->_e, 'failed to fetch remote style headers. Update aborted.', $this->url_themedata));
        }
        $this->remote = array_merge($this->remote, $theme);
        $this->remote_fetched = true;
        return $this->remote;
    }
    
    function validate_commit(){
        $this->remote = array_merge($this->remote, $this->get_remote_version());
        if($this->local['hash'] != $this->remote['hash'] || $this->local['date'] != $this->remote['date']){
            update_option($this->_o . '_temp_hash', $this->remote['hash']);
            update_option($this->_o . '_temp_date', $this->remote['date']);
            return true;
        }
        return false;
    }

    function get_remote_version(){
#    Fetch options from bitbucket API
        $response = wp_remote_get($this->url_commits);
        $response_code = wp_remote_retrieve_response_code($response);
        if(!is_wp_error($response) || $response_code === 200){
#            Parse the response for our variables
            $commit_history = json_decode($response['body']);
            if($commit_history->pagelen > 0){
                $last_commit = $commit_history->values[0];
                $hash = $this->branch . ":" . $last_commit->hash;
                $date = $last_commit->date;
                $remote = array('hash'=> $hash, 'date' => $date);
                $this->remote = array_merge($this->remote, $remote);
                return $remote;
            }else{
                return $this->error('zero_body_error', 
                new WP_Error($this->_e, 'get_remote_options returned zero body. Update aborted.'));
            }
        }
        return $this->error('could_not_connect', 
        new WP_Error($this->_e, 'get_remote_options could not connect. Update aborted.', $response_code));
    }

    function error($slug, $error){
        $errors = array_pull(get_settings_errors(), 'code');
        if(!in_array($slug, $errors))
            add_settings_error(ERR, $slug, ucwords(str_replace('_', ' ', $this->_e)) . ": " . $error->get_error_message());
        return false;
    }
}
