<?php
/*
    CBS Wordpress Theme Options
    
    Setting theme options which will help setup custom theme options
*/ 
function cbs_wordpress_customize_register($wp_customize){
$reg = reg();

/*******************************************************************************
    Add title / subtitle to site
*******************************************************************************/
#    Make the name & description update in our customizer onchange
    $wp_customize->get_setting( 'blogname' )->transport = 'postMessage';
    $wp_customize->get_setting( 'blogdescription' )->transport = 'postMessage';
#    Add 'display text' to site title section
    $wp_customize->add_setting("cbs_wordpress_title_display", array(
        'default'       => false,
        'capability'    => 'edit_theme_options',
        'type'          => 'option',
    ));
    $wp_customize->add_control("cbs_wordpress_title_display", array(
        'label'     => __('Display title/tagline in site header', DOMAIN),
        'section'   => 'title_tagline',
        'type'      => 'checkbox', 
    ));
#    Remove static front page setting, which is wonky
    $wp_customize->remove_section('static_front_page');

/*******************************************************************************
    Content Rotator Settings
    In combination with ACF, we work the content rotator
*******************************************************************************/
    $wp_customize->add_section("cbs_wordpress_content_rotator", array(
        'title'         => __('Main Content Rotator', DOMAIN),
        'description'   => "Manage the homepage content rotator",
        'priority'      => 50,
    ));
    $wp_customize->add_setting("cbs_wordpress_content_rotator[rotator_type]", array(
        'default'           => 'posts',
        'type'              => 'option',
        'capability'        => 'edit_theme_options',
    ));
    $wp_customize->add_control('cbs_wordpress_content_rotator[rotator_type]', array(
        'label'         => __('Carousel Type', DOMAIN),
        'section'       => 'cbs_wordpress_content_rotator',
        'settings'      => 'cbs_wordpress_content_rotator[rotator_type]',
        'type'          => 'radio',
        'choices'       => array(
            'none'      => 'None (Disabled)',
            'posts'     => "Last five posts in the 'Highlights' category, ordered by publication date",
        ),
    ));

/*******************************************************************************
    Social links
    These links are set up in functions.php under the 'social_media' reg setting
*******************************************************************************/
    $social_media = $reg->get('social_media');
    $wp_customize->add_section('cbs_wordpress_social_links', array(
        'title'         => __('Social Media Links', DOMAIN),
        'description'   => "Add your social media links. Leave blank to unset.",
        'priority'      => 80,
    ));
    foreach($social_media as $sm => $img_name){
        $key = preg_replace("/[^A-Za-z0-9 ]/", '', $sm);
        $wp_customize->add_setting("cbs_wordpress_social_media[$key]", array(
            'default'           => '',
            'type'              => 'option',
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'esc_url_raw',
        ));
        $wp_customize->add_control("cbs_wordpress_social_media[$key]", array(
            'label'     => __(ucfirst($sm) . " link", DOMAIN),
            'section'   => 'cbs_wordpress_social_links',
        ));
    }

/*******************************************************************************
    Column sizes
    NOTE: We require 2 columns, but can set them as two different sizes
    This may change in the future
*******************************************************************************/
    $wp_customize->add_section('cbs_wordpress_columns', array(
        'title'         => __('Column widths', DOMAIN),
        'priority'      => 35,
    ));
    $wp_customize->add_setting('cbs_wordpress_theme_options[column_widths]', array(
        'default'       => '8|4',
        'type'          => 'option',
        'capability'    => 'edit_theme_options',
    ));
    $wp_customize->add_control('cbs_wordpress_columns', array(
        'label'         => __('Column Widths', DOMAIN),
        'section'       => 'cbs_wordpress_columns',
        'settings'      => 'cbs_wordpress_theme_options[column_widths]',
        'type'          => 'radio',
        'choices'       => array(
            '8|4'       => 'Default (2:1 ratio)',
            '9|3'       => 'Wide content (3:1 ratio)',
            '6|6'       => 'Split (1:1 ratio)',
        ),
    ));
}

//  Add settings in the admin
include(DIR . '/inc/contact_info.php');

/*******************************************************************************
    Functions to operate the theme
*******************************************************************************/
//  Theme column widths
function cbs_wordpress_theme_columns(){
    global $_cbs_column_width;
    $setting_option = get_option("cbs_wordpress_theme_options");
    $setting = isset($setting_option['column_widths']) ? $setting_option['column_widths'] : '8|4';
    $_cbs_column_width = isset($_cbs_column_width) ? $_cbs_column_width : explode("|", $setting);
    echo sprintf("span%s", array_shift($_cbs_column_width));
}

function cbs_wordpress_theme_has_social_links(){
    $setting = array_filter(get_option("cbs_wordpress_social_media"));
    return !empty($setting);
}

//  Social media links
function cbs_wordpress_theme_social_link($type){
    $key = preg_replace("/[^A-Za-z0-9 \+]/", '', $type);
    $args = get_option("cbs_wordpress_social_media");
    $link = $args[$key];
    $img = reg()->get('social_media');
    $img = $img[$type];
    $path = get_stylesheet_directory() . "/img/social/" . $img;
    $url = get_template_directory_uri() . "/img/social/" . $img;
    $out = file_exists($path) ? sprintf("<img src='%s' alt='%s' title='%s'>", $url, $alt, ucfirst($type)) : ucfirst($type);
    if(!empty($link) && !empty($img) && filter_var($link, FILTER_VALIDATE_URL)){
?>
    <li><a href="<?=esc_url($link);?>" target="_blank"><?=$out;?></a></li>
<?php
    }
}
