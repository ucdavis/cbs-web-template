<?php
/*
    CBS Site templates
    Copyright The Regents of the University of California, Davis 
    all rights reserved
    
    Designed and built by Information & Educational Technology
    University of California, Davis
    
    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to
    deal in the Software without restriction, including without limitation the
    rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
    sell copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
    IN THE SOFTWARE.
    
    Site front page. This is made specifically for the CBS homepage. You may want
    to create your own here.

    @package WordPress
    @subpackage CBS Web Template

*/
get_header(); 
$news_cat = get_category_by_slug('news');
$news_cat_id = $news_cat ? $news_cat->cat_ID : '';
$news_link = get_category_link($news_cat_id);
?>
<div id="homepage-sections" class="main content row">
    <section id="news" class="<?php cbs_wordpress_theme_columns();?>">
        <h1 class="section-heading">News</h1>
        <a class="rss-link" href="<?=add_query_arg(array('feed' => 'rss2'), $news_link);?>" alt="RSS feed">
        <img src="<?php _i();?>/img/rss.png" alt="Link to news RSS feed"></a>
        <a href="<?=get_category_link_by_slug('news');?>" class="more-link">More News &rarr;</a>  

        <!--News loop-->
<?php
    $query = new WP_Query(array('cat' => $news_cat_id, 'posts_per_page' => 5));
    if($query->have_posts()):
        while($query->have_posts()):
            $query->the_post();
?>
        <article id="news-<?php the_id(); ?>" class="news-summary">
            <header>
                <a href="<?php the_permalink(); ?>">
                    <?php if(has_post_thumbnail()):?>
                    <?php the_post_thumbnail();?>
                    <?php endif;?>
                    <h2><?=get_the_title();?></h2>
                </a>
            </header>
            <p class="byline"><?php cbs_wordpress_entry_meta();?></p>
            <?php the_excerpt();?>
            <p><a href="<?php the_permalink();?>">Read More &rarr;</a></p>
        </article>
<?php      
        
        endwhile;   // end loop       
        ?>
          <!-- /.News loop -->
        <a class="more-link" href="<?=get_category_link_by_slug('news');?>">More news &rarr;</a>
<?php  else:   ?>
        <div id="no-news">
        No news at this time.
        </div>
<?php  endif;  // end if have_posts      
    wp_reset_postdata();
?>
    </section><!-- /.News -->
<?php get_sidebar();?>
    </div>
<?php get_footer();
