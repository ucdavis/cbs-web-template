<?php
/**
    CBS Site templates
    Copyright The Regents of the University of California, Davis 
    all rights reserved
    
    Designed and built by Information & Educational Technology
    University of California, Davis
    
    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to
    deal in the Software without restriction, including without limitation the
    rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
    sell copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
    IN THE SOFTWARE.

    Search Form

    @package WordPress
    @subpackage CBS Web Template

*/
?>
<form id="site-search" class="nav-search" type="get" action="<?php echo home_url( '/' ); ?>">
    <input name="s" id="search" class="search-query" type="text" placeholder="Search" value="<?php the_search_query(); ?>">
    <button class="btn hide" type="submit">Hit enter to search</button>
</form>

